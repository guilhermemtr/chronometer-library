#include "TimeLib.h"

TimeLib::TimeLib()
{
}

void TimeLib::start()
{
        this->startTime = std::chrono::system_clock::now();
}

void TimeLib::end()
{
        this->endTime = std::chrono::system_clock::now();
}

float TimeLib::getDuration()
{
    std::chrono::duration<float> seconds = this->endTime - this->startTime;
    return seconds.count();
}

TimeLib::~TimeLib()
{
}
